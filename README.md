## Requisitos del Proyecto

- PHP >= 7.3
- Composer
- XAMPP

## Iniciar el Proyecto
- Paso 1: Abra la terminal en su directorio raíz (api-cxc) y para instalar los paquetes del compositor, ejecute el siguiente comando:

```
composer install
```

- Paso 2: Para ejecutar las migraciones para la creacion de las tabla ejecute:

```
php artisan migrate
```

- Paso 3: Para ejecutar el llenado de la tabla con un registro inicial ejecute la semilla:

```
php artisan db:seed
```

## Generar Docs Swagger 

- Paso 4: Se debe generar la documentación con el fin de garantizar que la documentación este actualizada con los ultimos cambios. para ello ejecute:

```
php artisan l5-swagger:generate
```

Se tiene en cuenta que para acceder a esta documentación es mediante el endpoint: "api/documentation"   ejemplo:  http://localhost:8000/api/documentation


- Paso 5: Para ejecutar la aplicación (backend), debe ejecutar el siguiente comando en el directorio del proyecto. Esto le dará una dirección con el número de puerto (Ejemplo: 8000). Ahora navegue hasta la dirección indicada y verá que su aplicación se está ejecutando.

```
php artisan serve
```



