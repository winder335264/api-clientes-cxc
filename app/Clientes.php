<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Clientes extends Authenticatable
{
    use Notifiable;

    protected $table = "clientes";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'nombre' ,
        'apellidoPat',
        'apellidoMat', 
        'rfc',
        'fechanac',
        'created_at',
        'updated_at'
    ];

    
}
