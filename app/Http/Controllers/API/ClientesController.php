<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clientes;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;

/**
* @OA\Info(title="API Clientes CXC", version="1.0")
*
* @OA\Server(url="http://127.0.0.1:8000/api")
*/

class ClientesController extends Controller
{

    /**
    * @OA\Get(
    *     path="/clientes/listar",
    *     summary="Listar Clientes",
    *     @OA\Response(
    *         response=200,
    *         description="Mostrar todos los clientes"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function Listar(){
        
        $clientes = Clientes::select(['id','nombre', 'apellidoPat','apellidoMat','fechanac','rfc'])->orderBy('updated_at', 'desc')->get();
        return response()->json([
            'clientes' =>  $clientes,          
        ]);

    }

     /**
     *
     * @OA\post(path="/clientes/editar",
    *       summary = "Editar Cliente",
    *       description = "Endpoints para editar informacion del cliente",
    *   @OA\Parameter(
    *         name="id",
    *         in="query",
    *         description="ID del cliente",
    *         required=true,
     *      ),
    *   @OA\Parameter(
    *         name="nombre",
    *         in="query",
    *         description="Nombre del cliente",
    *         required=true,
     *      ),
    *   @OA\Parameter(
    *         name="apellidoPat",
    *         in="query",
    *         description="Apellido Paterno del cliente",
    *         required=true,
     *      ),
    *   @OA\Parameter(
    *         name="apellidoMat",
    *         in="query",
    *         description="Apellido Materno del cliente",
    *         required=true,
     *      ),
    *   @OA\Parameter(
    *         name="fechanac",
    *         in="query",
    *         description="Fecha de nacimiento  del cliente",
    *         required=true,
     *      ),
    *   @OA\Response(
    *         response="200",
    *         description="Cliente editado con exito"
    *     ),
    *   @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
     * )
     */
    public function Editar(Request $request){
        
    
        $clientes = Clientes::where('id', $request->id)->first();

        if($clientes){

            $clientes->fill([
                'nombre'        => $request->nombre,
                'apellidoPat'   => $request->apellidoPat,
                'apellidoMat'   => $request->apellidoMat,
                'fechanac'      => Carbon::parse($request->fechanac),
                'rfc'           => $this->GenerarRFC($request->nombre,$request->apellidoPat,$request->apellidoMat,date("d-m-Y", strtotime($request->fechanac)))
            ]);
    
            $clientes->save();
            return response()->json(['mensaje' =>'Cliente editado con éxito...','cliente' => $this->Listar()->original["clientes"], ],200);

        }else{

            return response()->json(['mensaje' => 'No se encontró el cliente en la BD...','clientes' => $this->Listar()->original["clientes"]], 200);

        }


    }

     /**
     *
     * @OA\post(path="/clientes/eliminar",
    *       summary = "Eliminar Cliente",
    *       description = "Endpoints para eliminar un cliente",
    *   @OA\Parameter(
    *         name="id",
    *         in="query",
    *         description="ID del cliente",
    *         required=true,
    *      ),
    *   @OA\Response(
    *         response="200",
    *         description="Cliente eliminado con exito"
    *     ),
    *   @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
     * )
     */

    public function Eliminar(Request $request){

        $cliente = Clientes::where('id', $request->id)->delete();

        if($cliente){
            return response()->json(['mensaje' => 'Cliente eliminado con exito...','clientes' => $this->Listar()->original["clientes"]], 200);
        }else{
            return response()->json(['mensaje' => 'No se encontró el cliente en la BD...','clientes' => $this->Listar()->original["clientes"]], 200);

        }
        
    }

  /**
     *
     * @OA\post(path="/clientes/crear",
    *       summary = "Crear Cliente",
    *       description = "Endpoints para crear un nuevo cliente",
    *   @OA\Parameter(
    *         name="nombre",
    *         in="query",
    *         description="Nombre del cliente",
    *         required=true,
     *      ),
    *   @OA\Parameter(
    *         name="apellidoPat",
    *         in="query",
    *         description="Apellido Paterno del cliente",
    *         required=true,
     *      ),
    *   @OA\Parameter(
    *         name="apellidoMat",
    *         in="query",
    *         description="Apellido Materno del cliente",
    *         required=true,
     *      ),
    *   @OA\Parameter(
    *         name="fechanac",
    *         in="query",
    *         description="Fecha de nacimiento del cliente",
    *         required=true,
     *      ),
    *   @OA\Response(
    *         response="200",
    *         description="Cliente creado con exito",
    *     ),
    *   @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error.",
    *     )
     * )
     */
   public function Crear(Request $request){

        $cliente = new Clientes();
        $cliente->nombre        = $request->nombre;
        $cliente->apellidoPat   = $request->apellidoPat;
        $cliente->apellidoMat   = $request->apellidoMat;
        $cliente->fechanac      = Carbon::parse($request->fechanac);
        $cliente->rfc           = $this->GenerarRFC($request->nombre,$request->apellidoPat,$request->apellidoMat,date("d-m-Y", strtotime($request->fechanac)));

        $cliente->save();
        return response()->json(['mensaje' => 'Cliente creado con éxito...','clientes' => $this->Listar()->original["clientes"]],200);
    }


   

    function GenerarRFC($nombre,$apellidoPaterno,$apellidoMaterno,$fecha)
    {
        
        $rfc="";
        /*Cambiamos todo a mayúsculas. Quitamos los espacios al principio y final del nombre y apellidos*/
        $nombre =strtoupper(trim($nombre));
        $apellidoPaterno =strtoupper(trim($apellidoPaterno));
        $apellidoMaterno =strtoupper(trim($apellidoMaterno));

        //Quitamos los artículos de los apellidos
        $apellidoPaterno = $this->QuitarArticulos($apellidoPaterno);
        $apellidoMaterno = $this->QuitarArticulos($apellidoMaterno);

        //Agregamos el primer caracter del apellido paterno
        $rfc = substr($apellidoPaterno,0, 1);

        //Buscamos y agregamos al rfc la primera vocal del primer apellido
        $len_apellidoPaterno=strlen($apellidoPaterno);

        for($x=1;$x<$len_apellidoPaterno;$x++)
        {
            $c=substr($apellidoPaterno,$x,1);

            if ($this->EsVocal($c))
            {
                $rfc .= $c;
                break;
            }
        }

        //Agregamos el primer caracter del apellido materno
        $rfc .= substr($apellidoMaterno,0, 1);

        //Agregamos el primer caracter del primer nombre
        $rfc .= substr($nombre,0, 1);

        //Agregamos la fecha yymmdd (por ejemplo: 19930920, 20 de Septiembre de 1993 )
        $rfc .= substr($fecha,8, 2).substr($fecha,3, 2).substr($fecha,0, 2);

        return $rfc;
    
    }

    function QuitarArticulos($palabra)
    {
        $palabra=str_replace("DEL ","",$palabra);
        $palabra=str_replace("LAS ","",$palabra);
        $palabra=str_replace("DE ","",$palabra);
        $palabra=str_replace("LA ","",$palabra);
        $palabra=str_replace("Y ","",$palabra);
        $palabra=str_replace("A ","",$palabra);
        return $palabra;
    }

    function EsVocal($letra)
    {
        if ($letra == 'A' || $letra == 'E' || $letra == 'I' || $letra == 'O' || $letra == 'U' || $letra == 'a' || $letra == 'e' || $letra == 'i' || $letra == 'o' || $letra == 'u')
        return 1;
        else
        return 0;
    }


}
