<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert(
            [
                'id' => '1',
                'nombre' => 'Winder Jose',
                'apellidoPat' => 'Morillo',
                'apellidoMat' => 'Torres',
                'rfc' => 'MOTW930920',
                'fechanac' =>  Carbon::parse('20-09-1993'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
            
        );
    }
}
