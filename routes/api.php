<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



/* Endponit Clientes   */
Route::get('clientes/listar', 'API\ClientesController@Listar');
Route::post('clientes/crear', 'API\ClientesController@Crear');
Route::post('clientes/editar', 'API\ClientesController@Editar');
Route::post('clientes/eliminar', 'API\ClientesController@Eliminar');


